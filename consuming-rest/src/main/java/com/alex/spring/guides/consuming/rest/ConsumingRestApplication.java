package com.alex.spring.guides.consuming.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ConsumingRestApplication {

    private static final Logger logger = LoggerFactory.getLogger(ConsumingRestApplication.class);

    public static void main(String[] args) {
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<Quote> entity = restTemplate.getForEntity("https://gturnquist-quoters.cfapps.io/api/random", Quote.class);
//        Quote body = entity.getBody();
//        System.out.println(entity.getStatusCode());
//        System.out.println(body);
        SpringApplication.run(ConsumingRestApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }

    @Bean
    public CommandLineRunner run(RestTemplate restTemplate) {
        return args -> {
            ResponseEntity<Quote> entity = restTemplate.getForEntity("https://gturnquist-quoters.cfapps.io/api/random", Quote.class);
            Quote body = entity.getBody();
            logger.info("Status = {}", entity.getStatusCode());
            logger.info("Bodu = {}", body);
        };
    }
}
