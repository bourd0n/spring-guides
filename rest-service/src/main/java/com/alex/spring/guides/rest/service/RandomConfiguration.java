package com.alex.spring.guides.rest.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.SecureRandom;
import java.util.Random;

@Configuration
public class RandomConfiguration {

    @Bean
    public Random getRandom() {
        return new SecureRandom();
    }
}
