package com.alex.spring.guides.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class GreetingService {

    private Random random;

    private final AtomicInteger counter = new AtomicInteger();
    @Autowired
    public GreetingService(Random random) {
        this.random = random;
    }

    @GetMapping(value = "/greeting")
//    @ResponseBody //not needed if @RestController
    public GreetingResponse greeting(@RequestParam(required = false, defaultValue = "World") String name) {
        return new GreetingResponse(
                //random.nextInt(),
                counter.incrementAndGet(),
                String.format("Hello, %s!", name)
        );
    }
}
