package com.alex.spring.guides.rest.service;

public class GreetingResponse {
    private final int id;
    private final String content;


    public GreetingResponse(int id, String content) {
        this.id = id;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
