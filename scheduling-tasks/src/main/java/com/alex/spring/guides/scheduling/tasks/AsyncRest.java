package com.alex.spring.guides.scheduling.tasks;

import com.alex.spring.guides.scheduling.tasks.quartz.SampleJob;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Map;

@RestController
public class AsyncRest {

    private static final Logger logger = LoggerFactory.getLogger(AsyncRest.class);

    @Autowired
    private Scheduler scheduler;

    @GetMapping("/async")
    @Async
    public void runAsync(@RequestParam("marker") String name) throws InterruptedException {
        logger.info("Async.start {}", name);
        if ("ok".equals(name)) {
            Thread.sleep(5000L);
        } else {
            throw new RuntimeException("ERROR for marker " + name);
        }
        logger.info("Async.finish {}", name);
    }

    @PostMapping("/addJob/{id}")
    public void addJob(@PathVariable("id") String id) throws SchedulerException {
        JobDetail job = JobBuilder.newJob(SampleJob.class)
                .setJobData(new JobDataMap(Collections.singletonMap("context-object", id)))
                .build();

        Trigger trigger = TriggerBuilder.newTrigger()
                .forJob(job)
                .startAt(Date.from(Instant.now().plus(15L, ChronoUnit.SECONDS)))
                .build();

        scheduler.scheduleJob(job, trigger);
    }

    @GetMapping("/**")
    public void redirect(HttpServletRequest req, HttpServletResponse response) throws IOException {
        response.sendRedirect("https://www.google.ru/" + req.getRequestURI() );
    }

    @PostMapping("/generic")
    public String genericPost(@RequestBody Map<String, Object> request,
                              @RequestHeader("channel") String channel) {
        System.out.println("Request " + request);
        System.out.println("Channel " + channel);
        return (String) request.get("a");
    }
}
