package com.alex.spring.guides.scheduling.tasks.quartz;

import org.quartz.JobBuilder;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.Date;

public class SampleJob extends QuartzJobBean {
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        System.out.println(new Date());
        System.out.println(context.getJobDetail().getJobDataMap().get("context-object"));
    }
}
