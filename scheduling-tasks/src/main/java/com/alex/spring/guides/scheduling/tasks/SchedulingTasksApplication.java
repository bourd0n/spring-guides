package com.alex.spring.guides.scheduling.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableScheduling
public class SchedulingTasksApplication  {

    private static final Logger logger = LoggerFactory.getLogger(SchedulingTasksApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SchedulingTasksApplication.class, args);
    }

    @Scheduled(initialDelay = 100L, fixedDelay = 5000L)
    public void scheduled() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Quote> entity = restTemplate.getForEntity("https://gturnquist-quoters.cfapps.io/api/random", Quote.class);
        Quote body = entity.getBody();
        logger.info("Status = {}", entity.getStatusCode());
        logger.info("Body = {}", body);
    }
}
